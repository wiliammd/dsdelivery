package com.devsuperior.dsdelivere.services;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devsuperior.dsdelivere.dto.OrderDTO;
import com.devsuperior.dsdelivere.dto.ProductDTO;
import com.devsuperior.dsdelivere.entities.Order;
import com.devsuperior.dsdelivere.entities.OrderStatus;
import com.devsuperior.dsdelivere.entities.Product;
import com.devsuperior.dsdelivere.repositorie.OrderRepository;
import com.devsuperior.dsdelivere.repositorie.ProductRepository;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderrepository;
	@Autowired
	private ProductRepository productrepository;
	
/*	injeção de dependência manual
 * public ProductService(ProductRepository repository) {
		this.repository = repository;
	}
*/
	@Transactional(readOnly = true)
	public List<OrderDTO> findAll(){
		List<Order> list = orderrepository.findOrderWithProducts();
		return list.stream().map(x -> new OrderDTO(x)).collect(Collectors.toList());
	}
	
	@Transactional
	public OrderDTO insert(OrderDTO dto){
		Order order = new Order(null, dto.getAddress(), dto.getLatitude(), dto.getLongitude(), Instant.now(), OrderStatus.PEDING);
		for(ProductDTO p: dto.getProducts()) {
			Product product = productrepository.getOne(p.getId());
			order.getProducts().add(product);
		}
		order = orderrepository.save(order);
		return new OrderDTO(order);
	}
	@Transactional
	public OrderDTO setDelivered(Long id){
		Order order = orderrepository.getOne(id);
		order.setStatus(OrderStatus.DELIVERED);
		order = orderrepository.save(order);
		return new OrderDTO(order);
		
	}
}
