import axios from 'axios';
const API_URL = 'https://wiliam-sds.herokuapp.com'
//exp://192.168.1.3:19000
//https://wiliam-sds.herokuapp.com
export function fetchOrders(){
    return axios(`${API_URL}/orders`)
}

export function confirmDelivery(orderId: number){
    return axios.put(`${API_URL}/orders/${orderId}/delivered`);
}
